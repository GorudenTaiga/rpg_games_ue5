// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "OnlineSubsystem.h"
#include "OnlineSubsystemUtils.h"
#include "Interfaces/OnlineSessionInterface.h"
#include "Kismet/GameplayStatics.h"
#include "EOS_GameInstance.generated.h"

/**
 * 
 */
UCLASS()
class RPG_API UEOS_GameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	UEOS_GameInstance();
	virtual void Init() override;

	UFUNCTION(BlueprintCallable, Category="EOS System")
	void LoginWithEOS();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category="EOS System")
	FString GetPlayerUsername();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category="EOS System")
	bool IsPlayerLoggedIn();



	void LoginWithEOS_Return(int32 LocalUserNum, bool bWasSuccess, const FUniqueNetId& UserId, const FString& ErrorMessage);

protected:
	IOnlineSubsystem* OnlineSubsystem;
	IOnlineIdentityPtr IdentityPointerRef;
};
