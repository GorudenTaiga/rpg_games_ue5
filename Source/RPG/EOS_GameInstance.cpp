// Fill out your copyright notice in the Description page of Project Settings.


#include "EOS_GameInstance.h"
#include "OnlineSubsystem.h"
#include "OnlineSubsystemUtils.h"
#include "Interfaces/OnlineSessionInterface.h"
#include "Kismet/GameplayStatics.h"

UEOS_GameInstance::UEOS_GameInstance()
{
}

void UEOS_GameInstance::Init()
{
	IdentityPointerRef = OnlineSubsystem->GetIdentityInterface();
	OnlineSubsystem = Online::GetSubsystem(this->GetWorld());
}

void UEOS_GameInstance::LoginWithEOS()
{ 
	if (OnlineSubsystem)
	{
		if (IdentityPointerRef)
		{
			FOnlineAccountCredentials AccountDetails;
			AccountDetails.Id = FString();
			AccountDetails.Token = FString();
			AccountDetails.Type = FString("accountportal");
			IdentityPointerRef->Login(0, AccountDetails);
			IdentityPointerRef->OnLoginCompleteDelegates->AddUObject(this, UEOS_GameInstance::LoginWithEOS_Return);
		}
	}
}

FString UEOS_GameInstance::GetPlayerUsername()
{
	if (OnlineSubsystem)
	{
		if (IdentityPointerRef)
		{
			if (IdentityPointerRef->GetLoginStatus(0) == ELoginStatus::LoggedIn)
			{
				return IdentityPointerRef->GetPlayerNickname(0);
			}
		}
	}
	return FString();
}

bool UEOS_GameInstance::IsPlayerLoggedIn()
{
	if (OnlineSubsystem)
	{
		if (IdentityPointerRef)
		{
			if (IdentityPointerRef->GetLoginStatus(0) == ELoginStatus::LoggedIn)
			{
				return true;
			}
		}
	}
	return false;
}


void UEOS_GameInstance::LoginWithEOS_Return(int32 LocalUserNum, bool bWasSuccess, const FUniqueNetId& UserId, const FString& ErrorMessage)
{
	if (bWasSuccess)
	{
		UE_LOG(LogTemp, Warning, TEXT("Login Berhasil"));
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("Login Gagal"));
	}
}
